package com.xmppinstantmessenger;

import android.app.Notification;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.xmppinstantmessenger.fachkonzept.XMPPChatConnection;
import com.xmppinstantmessenger.oberflaeche.ContactListActivity;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import java.io.IOException;

/**
 * XMPPConnectionService
 *
 * Ein Service / Hintergrund-Thread, der eine XMPPChatConnection erstellt und diese im Hintergrund
 * laufen lässt.
 *
 * Dient außerdem als Liste für alle neu erzeugten Broadcasts.
 *
 * Author: Patrick Swierzy
 * Zuletzt bearbeitet: 05.05.2019
 */
public class XMPPConnectionService extends Service {

    private static final String TAG = "XMPPConnService";

    //Broadcast messages
    public static final String UI_AUTHENTICATED = "com.xmppinstantmessenger.uiauthenticated";
    public static final String SEND_MESSAGE = "com.xmppinstantmessenger.sendmessage";
    public static final String BUNDLE_MESSAGE_BODY = "b_body";
    public static final String BUNDLE_TO = "b_to";
    public static final String BUNDLE_TIME = "b_time";
    public static final String NEW_MESSAGE = "com.xmppinstantmessenger.newmessage";
    public static final String BUNDLE_FROM_JID = "b_from";
    public static final String ADD_CONTACT = "com.xmppinstantmessenger.addcontact";
    public static final String NEW_CONTACT = "com.xmppinstantmessenger.newcontact";
    public static final String UPDATE_CONTACT = "com.xmppinstantmessenger.updatecontact";
    public static final String DELETE_CONTACT = "com.xmppinstantmessenger.deletecontact";
    public static final String RECEIVE_CONTACTS = "com.xmppinstantmessenger.getcontacts";
    public static final String CONTACT_JID = "c_jid";
    public static final String CONTACT_NICK = "c_nick";
    public static final String CONTACT_AVATAR = "c_avatar";

    //XEP-0313: MAM (Message Archive)
    public static final String RECEIVE_HISTORY = "com.xmppinstantmessenger.receivehistory";


    public static XMPPChatConnection.ConnectionState sConnectionState;
    public static XMPPChatConnection.LoggedInState sLoggedInState;

    private boolean active; //Thread aktiv oder nicht
    private Thread thread;
    private Handler threadHandler; //Wird benutzt um mit dem Thread zu kommunizieren
    private XMPPChatConnection connection;


    public XMPPConnectionService() {

    }

    public static XMPPChatConnection.ConnectionState getState()
    {
        if (sConnectionState == null)
        {
            return XMPPChatConnection.ConnectionState.DISCONNECTED;
        }
        return sConnectionState;
    }

    public static XMPPChatConnection.LoggedInState getLoggedInState()
    {
        if (sLoggedInState == null)
        {
            return XMPPChatConnection.LoggedInState.LOGGED_OUT;
        }
        return sLoggedInState;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG,"onCreate()");

        if (Build.VERSION.SDK_INT >= 26) {
            Notification notification = new NotificationCompat.Builder(this, "XMPPMSNGR")
                    .setContentTitle("")
                    .setContentText("").build();

            startForeground(1, notification);
        }
    }

    private void initConnection() {
        Log.d(TAG, "initConnection()");
        if(connection == null) {
            connection = new XMPPChatConnection(this);
        }
        try {
            connection.connect();
        } catch (InterruptedException | IOException | SmackException | XMPPException e) {
            Log.d(TAG, "Something went wrong while connecting, make sure the credentials are right and try again.");
            e.printStackTrace();
            //Stop service
            stopSelf();
        }
    }

    public void start() {
        Log.d(TAG,"Service start()");

        if(!active) {
            active = true;
            if(thread == null || !thread.isAlive()) {
                thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //Looper ist sowas wie eine Eventloop
                        //https://developer.android.com/reference/android/os/Looper.html#prepare%28%29

                        Looper.prepare();

                        //Der Handler gehört dem Thread indem er erzeugt wurde
                        threadHandler = new Handler();
                        initConnection();
                        //Code here is run in a background thread
                        Looper.loop();
                    }
                });
                thread.start();
            }
        }
    }

    public void stop() {
        Log.d(TAG, "stop()");
        active = false;
        threadHandler.post(new Runnable() {
            @Override
            public void run() {
                if(connection != null)
                    //TODO: Shut down connection
                    connection.disconnect();
            }
        });
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand()");
        start();
        return Service.START_STICKY;
        //RETURNING START_STICKY CAUSES OUR CODE TO STICK AROUND WHEN THE APP ACTIVITY HAS DIED.
    }

    @Override
    public void onDestroy() {
        //Service wurde gekillt, nicht per Benutzereingabe

        Log.d(TAG, "onDestroy(), restarting service");
        super.onDestroy();

        //Versuche den Service gleich wieder zu starten
        //Intent i = new Intent(this, RestartService.class);
        //sendBroadcast(i);

        stop();
    }


}
