package com.xmppinstantmessenger.datenhaltung;

public class BaseMessage {
    public String message;
    public BaseUser sender;
    public long createdAt;

    public BaseMessage(String message, BaseUser sender, long createdAt) {
        this.message = message;
        this.sender = sender;
        this.createdAt = createdAt;
    }
}
