package com.xmppinstantmessenger.datenhaltung;

public class BaseUser {
    public String nickname;
    public String profileUrl;
    public byte[] avatar;
    public String jid;

    public BaseUser(String nickname, String profileUrl, String jid, byte[] avatar) {
        this.nickname = nickname;
        this.profileUrl = profileUrl;
        this.jid = jid;
        this.avatar = avatar;
    }
}
