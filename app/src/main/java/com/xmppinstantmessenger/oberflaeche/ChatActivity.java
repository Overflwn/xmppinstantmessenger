package com.xmppinstantmessenger.oberflaeche;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.xmppinstantmessenger.R;
import com.xmppinstantmessenger.XMPPConnectionService;
import com.xmppinstantmessenger.datenhaltung.BaseMessage;
import com.xmppinstantmessenger.datenhaltung.BaseUser;
import com.xmppinstantmessenger.fachkonzept.ChatRecyclerViewAdapter;
import com.xmppinstantmessenger.fachkonzept.XMPPChatConnection;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Chat-Activity
 *
 * Eine vertikal scrollbare Liste von Nachrichten (text).
 *
 * Füllt die Liste basierend auf 2 ArrayLists (jeweils Usernamen und Links zu Profilbildern)
 *
 * Author: Patrick Swierzy
 * Zuletzt bearbeitet: 05.05.2019
 * Notizen:
 * - https://www.youtube.com/watch?v=Vyqz_-sJGFk
 */
public class ChatActivity extends AppCompatActivity {

    private static final String TAG="ChatActivity";

    private RecyclerView messageRecycler;
    private ChatRecyclerViewAdapter messageAdapter;
    private Button sendButton;
    private EditText textBox;
    private String contactJid;
    //Dieses Element dient nur zu Vergleichszwecken.
    private BaseUser me;
    private BaseUser sender;
    private List<BaseMessage> messageList;
    private BroadcastReceiver broadcastReceiver;


    @Override
    protected void onPause() {
        // Eine andere Activity geht an oder die App wird gewechselt / geschlossen / das Handy geht
        // in den Standby Modus.
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onResume() {
        // Es wird wieder in diese Activity gewechselt.
        super.onResume();
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                switch(action) {
                    case XMPPConnectionService.NEW_MESSAGE:
                        Log.d(TAG, "Got a message");
                        String from = intent.getStringExtra(XMPPConnectionService.BUNDLE_FROM_JID);
                        String body = intent.getStringExtra(XMPPConnectionService.BUNDLE_MESSAGE_BODY);
                        long time = intent.getLongExtra(XMPPConnectionService.BUNDLE_TIME, (new Date()).getTime());
                        if(from.equals(sender.jid)) {
                            messageList.add(new BaseMessage(body, sender, time));
                            messageRecycler.setAdapter(messageAdapter);
                        }else if(from.equals(me.jid)) {
                            //TODO: bessere variante für history finden
                            messageList.add(new BaseMessage(body, me, time));
                            messageRecycler.setAdapter(messageAdapter);
                        }else {
                            Log.d(TAG, "Got a message from " + from);
                        }
                        return;
                }
            }
        };
        IntentFilter filter = new IntentFilter(XMPPConnectionService.NEW_MESSAGE);
        registerReceiver(broadcastReceiver,filter);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Die Activity wird gestartet.
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Chat");
        setSupportActionBar(toolbar);

        sendButton = findViewById(R.id.button_chatbox_send);
        textBox = findViewById(R.id.edittext_chatbox);


        messageList = new ArrayList<BaseMessage>();

        Intent intent = getIntent();
        contactJid = intent.getStringExtra("EXTRA_CONTACT_JID");
        String nickname = intent.getStringExtra("EXTRA_CONTACT_NICK");
        setTitle(nickname);
        toolbar.setTitle(nickname);

        //Find jid in contact list and get the BaseUser object
        for(BaseUser user : ContactListActivity.users) {
            if (user.jid.equals(contactJid)) {
                sender = user;
                break;
            }
        }

        //Vergleichszwecke
        me = new BaseUser("me", null, PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
                .getString("xmpp_jid", null), null);

        messageRecycler = (RecyclerView) findViewById(R.id.recyclerViewChat);
        messageAdapter = new ChatRecyclerViewAdapter(this, messageList);
        LinearLayoutManager layout = new LinearLayoutManager(this);
        //immer unterstes Item zeigen
        layout.setStackFromEnd(true);
        messageRecycler.setLayoutManager(layout);
        messageRecycler.setAdapter(messageAdapter);

        sendButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Only send if connected

                if(XMPPConnectionService.getState().equals(XMPPChatConnection.ConnectionState.CONNECTED)) {
                    //Nur wenn was eingetippt wurde
                    if(textBox.getText() != null && !textBox.getText().toString().isEmpty()) {
                        Log.d(TAG, "Sending message...");

                        Intent intent = new Intent(XMPPConnectionService.SEND_MESSAGE);
                        intent.putExtra(XMPPConnectionService.BUNDLE_MESSAGE_BODY,
                                textBox.getText().toString());
                        intent.putExtra(XMPPConnectionService.BUNDLE_TO, contactJid);

                        sendBroadcast(intent);

                        //Update chat view
                        messageList.add(new BaseMessage(textBox.getText().toString(), me, (new Date()).getTime()));
                        messageRecycler.setAdapter(messageAdapter);

                        textBox.getText().clear();
                    }
                }
            }
        });



        intent = new Intent(XMPPConnectionService.RECEIVE_HISTORY);
        intent.putExtra(XMPPConnectionService.BUNDLE_TO, contactJid);
        sendBroadcast(intent);

        Log.d(TAG, "ChatActivity started.");
    }
}
