package com.xmppinstantmessenger.oberflaeche;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;

import com.xmppinstantmessenger.LoginActivity;
import com.xmppinstantmessenger.R;
import com.xmppinstantmessenger.XMPPConnectionService;
import com.xmppinstantmessenger.datenhaltung.BaseMessage;
import com.xmppinstantmessenger.datenhaltung.BaseUser;
import com.xmppinstantmessenger.fachkonzept.ContactsRecyclerViewAdapter;
import com.xmppinstantmessenger.fachkonzept.SwipeToDeleteCallback;

import java.util.ArrayList;
import java.util.Date;

/**
 * Kontakte-Activity
 *
 * Eine vertikal scrollbare Liste von Kontakten (Bild + Text)
 *
 * Füllt die Liste basierend auf 2 ArrayLists (jeweils Usernamen und Links zu Profilbildern)
 *
 * Author: Patrick Swierzy
 * Zuletzt bearbeitet: 02.04.2019
 * Notizen:
 * - https://www.youtube.com/watch?v=Vyqz_-sJGFk
 */
public class ContactListActivity extends AppCompatActivity {

    private static final String TAG="ContactListActivity";

    //Liste der User
    public static ArrayList<BaseUser> users = new ArrayList<>();

    private BroadcastReceiver broadcastReceiver;

    private ContactsRecyclerViewAdapter adapter;

    @Override
    protected void onPause() {
        // Eine andere Activity geht an oder die App wird gewechselt / geschlossen / das Handy geht
        // in den Standby Modus.
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onResume() {
        // Es wird wieder in diese Activity gewechselt.
        super.onResume();

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                switch(action) {
                    case XMPPConnectionService.NEW_CONTACT: {

                        String nickname = intent.getStringExtra(XMPPConnectionService.CONTACT_NICK);

                        String jid = intent.getStringExtra(XMPPConnectionService.CONTACT_JID);
                        Log.d(TAG, "Got a contact " + jid);
                        byte[] avatar = intent.getByteArrayExtra(XMPPConnectionService.CONTACT_AVATAR);
                        boolean found = false;

                        //TODO: BUG: broadcast wird zwei Male erhalten, weshalb wir stattdessen jetzt bei Duplikaten das alte Profil aktualisieren
                        for (BaseUser usr : users) {
                            if (usr.jid.equals(jid)) {
                                usr.nickname = nickname;
                                usr.avatar = avatar;
                                found = true;

                                break;
                            }
                        }

                        //Wenn es ein neuer Benutzer ist, dann neues BaseUser Objekt mit den gegebenen
                        //Daten erstellen. Die profileUrl wird als "fallback" verwendet, falls avatar
                        //null ist.
                        if (!found)
                            users.add(new BaseUser(nickname, "https://www.acspri.org.au/sites/acspri.org.au/files/profile-placeholder.png", jid, avatar));
                        RecyclerView recyclerView = findViewById(R.id.recyclerView);
                        recyclerView.setAdapter(adapter);
                        return;
                    }
                    case XMPPConnectionService.DELETE_CONTACT: {
                        String jid = intent.getStringExtra(XMPPConnectionService.CONTACT_JID);
                        Log.d(TAG, "Deleting contact: " + jid);
                        for (BaseUser user : users) {
                            if(user.jid.equals(jid)) {
                                int pos = users.indexOf(user);
                                users.remove(user);
                                RecyclerView recyclerView = findViewById(R.id.recyclerView);
                                recyclerView.getAdapter().notifyItemRemoved(pos);
                                break;
                            }
                        }
                        return;
                    }
                }
            }
        };
        IntentFilter filter = new IntentFilter(XMPPConnectionService.NEW_CONTACT);
        filter.addAction(XMPPConnectionService.DELETE_CONTACT);
        registerReceiver(broadcastReceiver,filter);
    }

    /*
    Wird beim erstellen der Activity ausgeführt
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Kontakte");
        setSupportActionBar(toolbar);



        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: Adding contact.");
                // Starte "Chat"-Activity
                Intent intent = new Intent(getApplicationContext(), AddContactActivity.class);
                startActivity(intent);
            }
        });

        Log.d(TAG, "onCreate: started.");

        //Beim Start erstmal alle Kontakte holen
        Intent intent = new Intent(XMPPConnectionService.RECEIVE_CONTACTS);
        sendBroadcast(intent);



        // Initialisiere die Listen & RecyclerView
        init();
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Really Exit?")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        stopService(LoginActivity.xmppService);
                        ContactListActivity.super.onBackPressed();
                    }
                }).create().show();
    }

    private void init() {
        Log.d(TAG, "init: started.");

        initRecyclerView();
    }

    private void initRecyclerView() {
        Log.d(TAG, "initRecyclerView: started.");
        RecyclerView recyclerView = findViewById(R.id.recyclerView);

        // Erstelle den Adapter
        adapter = new ContactsRecyclerViewAdapter(this);
        recyclerView.setAdapter(adapter);

        // Standard, keine ahnung was das machen soll
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new SwipeToDeleteCallback(adapter));
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

}
