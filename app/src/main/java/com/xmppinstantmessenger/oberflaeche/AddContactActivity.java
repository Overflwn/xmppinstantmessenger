package com.xmppinstantmessenger.oberflaeche;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.xmppinstantmessenger.R;
import com.xmppinstantmessenger.XMPPConnectionService;

public class AddContactActivity extends AppCompatActivity {
    private static final String TAG = "AddContactActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        Button addButton = findViewById(R.id.addButton);
        final TextInputLayout jid = findViewById(R.id.jabberID);
        final TextInputLayout nick = findViewById(R.id.nickname);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(XMPPConnectionService.ADD_CONTACT);
                intent.putExtra(XMPPConnectionService.CONTACT_JID, jid.getEditText().getText().toString());
                intent.putExtra(XMPPConnectionService.CONTACT_NICK, nick.getEditText().getText().toString());
                Log.d(TAG, "Adding contact: " + jid.getEditText().getText().toString());
                sendBroadcast(intent);
                finish();
            }
        });
    }
}
