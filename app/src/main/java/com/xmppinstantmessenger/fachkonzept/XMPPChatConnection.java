package com.xmppinstantmessenger.fachkonzept;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.xmppinstantmessenger.R;
import com.xmppinstantmessenger.XMPPConnectionService;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.ReconnectionListener;
import org.jivesoftware.smack.ReconnectionManager;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.chat2.IncomingChatMessageListener;
import org.jivesoftware.smack.packet.ExtensionElement;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterEntry;
import org.jivesoftware.smack.roster.RosterListener;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.delay.packet.DelayInformation;
import org.jivesoftware.smackx.mam.MamManager;
import org.jivesoftware.smackx.vcardtemp.VCardManager;
import org.jivesoftware.smackx.vcardtemp.packet.VCard;
import org.jxmpp.jid.BareJid;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.Jid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.List;


/**
 * XMPPChatConnection
 *
 * Erstellt eine Verbindung zum XMPP Server mithilfe der gegebenen Login-Daten.
 * Versendet Nachrichten und schickt einen Broadcast bei Empfang.
 * Außerdem ermöglicht es die History vom MAM zu laden.
 *
 * Author: Patrick Swierzy
 * Zuletzt bearbeitet: 05.05.2019
 */
public class XMPPChatConnection implements ConnectionListener {

    private static final String TAG = "XMPPChatConnection";

    private final Context applicationContext;
    private final String username;
    private final String password;
    private final String serviceName;
    private XMPPTCPConnection connection;
    private Roster roster;
    private VCardManager vcards;

    private BroadcastReceiver uiThreadMessageReceiver;

    private Collection<RosterEntry> entries;

    public static enum ConnectionState {
        CONNECTED, AUTHENTICATED, CONNECTING, DISCONNECTING, DISCONNECTED
    }

    public static enum LoggedInState {
        LOGGED_IN, LOGGED_OUT;
    }



    /**
     * Create a new Background XMPP Connection
     *
     * This is supposed to be used in a seperate Service / Thread
     * @param context
     */
    public XMPPChatConnection(Context context) {
        Log.d(TAG, "XMPPChatConnection()");
        applicationContext = context.getApplicationContext();
        String jid = PreferenceManager.getDefaultSharedPreferences(applicationContext)
                .getString("xmpp_jid", null);
        password = PreferenceManager.getDefaultSharedPreferences(applicationContext)
                .getString("xmpp_password", null);

        if(jid != null) {
            username = jid.split("@")[0];
            serviceName = jid.split("@")[1];
        }else {
            username = "";
            serviceName = "";
        }
    }

    /**
     * Connect to the server
     * @throws IOException
     * @throws XMPPException
     * @throws SmackException
     */
    public void connect() throws IOException, XMPPException, SmackException, InterruptedException {
        Log.d(TAG, "Connecting to server " + serviceName);
        XMPPTCPConnectionConfiguration conf = XMPPTCPConnectionConfiguration.builder()
                .setUsernameAndPassword(username, password)
                .setXmppDomain(serviceName)
                .setResource("xmppinstantmessenger")
                .build();

        //Set up the UI thread broadcast message receiver
        setupUiThreadBroadCastMessageReceiver();

        connection = new XMPPTCPConnection(conf);
        connection.addConnectionListener(this);
        connection.connect();
        Log.d(TAG, "Connection established");
        roster = Roster.getInstanceFor(connection);


        roster.setSubscriptionMode(Roster.SubscriptionMode.accept_all);

        connection.login();

        ChatManager.getInstanceFor(connection).addIncomingListener(new IncomingChatMessageListener() {
            @Override
            public void newIncomingMessage(EntityBareJid messageFrom, Message message, Chat chat) {
                NotificationCompat.Builder builder = new NotificationCompat.Builder(applicationContext, "XMPPMSNGR")
                        .setSmallIcon(R.drawable.circle)
                        .setContentTitle("XMPP Instant Messenger")
                        .setContentText("You've got a new message!")
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                NotificationManagerCompat notificationManager = NotificationManagerCompat.from(applicationContext);
                notificationManager.notify(2, builder.build());


                //Added
                Log.d(TAG, "message.getBody(): " + message.getBody());
                Log.d(TAG, "message.getFrom(): " + message.getFrom());

                String from = message.getFrom().toString();
                long time = (new Date()).getTime();
                DelayInformation inf = message.getExtension("x","jabber:x:delay");
                if(inf != null)
                    time = inf.getStamp().getTime();

                String contactJid;
                if(from.contains("/")) {
                    contactJid = from.split("/")[0];
                    Log.d(TAG, "The real jid is : " + contactJid);
                    Log.d(TAG, "The message is from: " + from);
                }else {
                    contactJid = from;
                }

                //Bundle up intent and send the broadcast
                Intent intent = new Intent(XMPPConnectionService.NEW_MESSAGE);
                intent.setPackage(applicationContext.getPackageName());
                intent.putExtra(XMPPConnectionService.BUNDLE_FROM_JID, contactJid);
                intent.putExtra(XMPPConnectionService.BUNDLE_MESSAGE_BODY, message.getBody());
                intent.putExtra(XMPPConnectionService.BUNDLE_TIME, time);

                applicationContext.sendBroadcast(intent);
                Log.d(TAG, "Received message from: " + contactJid + " broadcest sent.");
            }
        });

        ReconnectionManager reconnectionManager = ReconnectionManager.getInstanceFor(connection);

        reconnectionManager.addReconnectionListener(new ReconnectionListener() {

            @Override
            public void reconnectingIn(int seconds) {
                XMPPConnectionService.sConnectionState = ConnectionState.CONNECTING;
                Log.d(TAG,"ReconnectingIn() ");
            }

            @Override
            public void reconnectionFailed(Exception e) {
                XMPPConnectionService.sConnectionState = ConnectionState.DISCONNECTED;
                Log.d(TAG,"ReconnectionFailed()");
            }
        });
        reconnectionManager.enableAutomaticReconnection();
        vcards = VCardManager.getInstanceFor(connection);

        roster.addRosterListener(new RosterListener() {
            @Override
            public void entriesAdded(Collection<Jid> addresses) {
                // Sende neue Kontakte mit nickname, jid und avatar als broadcast
                if(!connection.isAuthenticated())
                    return;
                for(Jid jid : addresses) {
                    RosterEntry entry = roster.getEntry(jid.asBareJid());
                    if(entries != null)
                        entries.add(entry);
                    //Bundle up intent and send the broadcast
                    Intent intent = new Intent(XMPPConnectionService.NEW_CONTACT);
                    intent.setPackage(applicationContext.getPackageName());
                    intent.putExtra(XMPPConnectionService.CONTACT_JID, jid.toString());
                    if(entry.getName() != null && !entry.getName().isEmpty())
                        intent.putExtra(XMPPConnectionService.CONTACT_NICK, entry.getName());
                    else
                        intent.putExtra(XMPPConnectionService.CONTACT_NICK, jid.toString());

                    try {
                        if(vcards.isSupported(jid.asBareJid())) {
                            VCard card = vcards.loadVCard(jid.asEntityBareJidOrThrow());
                            byte[] avatar = card.getAvatar();
                            intent.putExtra(XMPPConnectionService.CONTACT_AVATAR, avatar);

                        }
                    } catch (SmackException.NoResponseException e) {
                        e.printStackTrace();
                    } catch (XMPPException.XMPPErrorException e) {
                        e.printStackTrace();
                    } catch (SmackException.NotConnectedException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Log.d(TAG, "New roster entry: " + jid.toString());
                    applicationContext.sendBroadcast(intent);
                }

            }

            @Override
            public void entriesUpdated(Collection<Jid> addresses) {
                // Sende Kontakte mit nickname, jid und avatar als broadcast
                for(Jid jid : addresses) {

                    RosterEntry entry = roster.getEntry(jid.asBareJid());
                    if(entries != null) {
                        for(RosterEntry e : entries) {
                            if (e.getJid().toString().equals(jid)) {
                                entries.remove(e);
                                entries.add(e);
                            }
                        }
                    }

                    //Bundle up intent and send the broadcast
                    Intent intent = new Intent(XMPPConnectionService.UPDATE_CONTACT);
                    intent.setPackage(applicationContext.getPackageName());
                    intent.putExtra(XMPPConnectionService.CONTACT_JID, jid.toString());
                    intent.putExtra(XMPPConnectionService.CONTACT_NICK, entry.getName());
                    try {
                        VCard card = vcards.loadVCard(jid.asEntityBareJidOrThrow());
                        if(card != null) {
                            byte[] avatar = card.getAvatar();
                            intent.putExtra(XMPPConnectionService.CONTACT_AVATAR, avatar);
                        }
                    } catch (SmackException.NoResponseException e) {
                        e.printStackTrace();
                    } catch (XMPPException.XMPPErrorException e) {
                        e.printStackTrace();
                    } catch (SmackException.NotConnectedException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    applicationContext.sendBroadcast(intent);
                }
            }

            @Override
            public void entriesDeleted(Collection<Jid> addresses) {
                // Sende Kontakte mit jid als broadcast
                for(Jid jid : addresses) {
                    if(entries != null) {
                        for (RosterEntry e : entries) {
                            if (e.getJid().toString().equals(jid)) {
                                entries.remove(e);
                            }
                        }
                    }
                    //Bundle up intent and send the broadcast
                    Intent intent = new Intent(XMPPConnectionService.DELETE_CONTACT);
                    intent.setPackage(applicationContext.getPackageName());
                    intent.putExtra(XMPPConnectionService.CONTACT_JID, jid.toString());
                    applicationContext.sendBroadcast(intent);
                }
            }

            @Override
            public void presenceChanged(Presence presence) {

            }
        });
        /*NotificationCompat.Builder builder = new NotificationCompat.Builder(applicationContext, "XMPPMSNGR")
                .setSmallIcon(R.drawable.circle)
                .setContentTitle("XMPP Instant Messenger")
                .setContentText("Connected.")
                .setOngoing(true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(applicationContext);

        notificationManager.notify(0, builder.build());*/
    }

    /**
     * Disconnect from the server
     */
    public void disconnect() {
        Log.d(TAG, "Disconnecting from server " + serviceName);
        try {
            if(connection != null)
                connection.disconnect();
        }catch(Exception e) {

            e.printStackTrace();
        }
        XMPPConnectionService.sConnectionState = ConnectionState.DISCONNECTED;
        connection = null;

        //Kill status notification
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(applicationContext);
        notificationManager.cancel(0);
    }

    /**
     * Send a broadcast to the ContactList Activity
     */
    private void showContactListActivityWhenAuthenticated() {
        Intent i = new Intent(XMPPConnectionService.UI_AUTHENTICATED);
        i.setPackage(applicationContext.getPackageName());
        applicationContext.sendBroadcast(i);
        Log.d(TAG, "Sent the broadcast that we are authenticated!");
    }

    private void addContact(String jid, String nickname) {
        try {
            roster.createEntry(JidCreate.bareFrom(jid), nickname, new String[] {"contacts"});
            roster.sendSubscriptionRequest(JidCreate.bareFrom(jid));
            Log.d(TAG, "Added " + jid + " to roster.");
        } catch (SmackException.NotLoggedInException e) {
            Log.d(TAG, "Couldn't add contact: Not logged in.");
            e.printStackTrace();
        } catch (SmackException.NotConnectedException e) {
            Log.d(TAG, "Couldn't add contact: Not connected.");
            e.printStackTrace();
        } catch (InterruptedException e) {
            Log.d(TAG, "Couldn't add contact: Interrupted.");
            e.printStackTrace();
        } catch (XmppStringprepException e) {
            Log.d(TAG, "Couldn't add contact: Jid has wrong formatting (" + jid + ").");
            e.printStackTrace();
        } catch (XMPPException.XMPPErrorException e) {
            e.printStackTrace();
        } catch (SmackException.NoResponseException e) {
            e.printStackTrace();
        }
    }

    private void removeContact(String jid) {
        try {
            RosterEntry entry = roster.getEntry(JidCreate.bareFrom(jid));
            if (entry != null) {
                roster.removeEntry(entry);
                Log.d(TAG, "Removed contact " + jid);
            }
        } catch (SmackException.NotLoggedInException e) {
            Log.d(TAG, "Couldn't delete contact: Not logged in.");
            e.printStackTrace();
        } catch (SmackException.NoResponseException e) {
            Log.d(TAG, "Couldn't delete contact: No response.");
            e.printStackTrace();
        } catch (XMPPException.XMPPErrorException e) {
            Log.d(TAG, "Couldn't delete contact:XMPP Error.");
            e.printStackTrace();
        } catch (SmackException.NotConnectedException e) {
            Log.d(TAG, "Couldn't delete contact: Not connected.");
            e.printStackTrace();
        } catch (InterruptedException e) {
            Log.d(TAG, "Couldn't delete contact: Interrupted.");
            e.printStackTrace();
        } catch (XmppStringprepException e) {
            Log.d(TAG, "Couldn't delete contact: JID not formatted correctly.");
            e.printStackTrace();
        }
    }

    /**
     * Listen to broadcasts from other Activities / Threads
     */
    private void setupUiThreadBroadCastMessageReceiver() {
        Log.d(TAG, "Setting broadcast receiver.");
        uiThreadMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                switch(action) {
                    case XMPPConnectionService.SEND_MESSAGE:
                        sendMessage(intent.getStringExtra(XMPPConnectionService.BUNDLE_MESSAGE_BODY),
                                intent.getStringExtra(XMPPConnectionService.BUNDLE_TO));
                        break;
                    case XMPPConnectionService.RECEIVE_HISTORY:
                        receiveHistory(intent.getStringExtra(XMPPConnectionService.BUNDLE_TO));
                        break;
                    case XMPPConnectionService.RECEIVE_CONTACTS:
                        receiveContacts();
                        break;
                    case XMPPConnectionService.ADD_CONTACT:
                        addContact(intent.getStringExtra(XMPPConnectionService.CONTACT_JID),intent.getStringExtra(XMPPConnectionService.CONTACT_NICK));
                        break;
                    case XMPPConnectionService.DELETE_CONTACT:
                        removeContact(intent.getStringExtra(XMPPConnectionService.CONTACT_JID));
                }
            }
        };
        IntentFilter filter = new IntentFilter();
        filter.addAction(XMPPConnectionService.SEND_MESSAGE);
        //XEP-0313: MAM (Message Archive)
        filter.addAction(XMPPConnectionService.RECEIVE_HISTORY);

        filter.addAction(XMPPConnectionService.RECEIVE_CONTACTS);

        filter.addAction(XMPPConnectionService.ADD_CONTACT);

        filter.addAction(XMPPConnectionService.DELETE_CONTACT);

        applicationContext.registerReceiver(uiThreadMessageReceiver, filter);
        Log.d(TAG, "Broadcast receiver set.");
    }

    /**
     * Try to get the message history with that specific contact through MAM.
     * TODO: Maybe move the whole message archiving to a local database. (Which gives us timestamps)
     * @param contactJid
     */
    private void receiveHistory(String contactJid) {
        Log.d(TAG, "Getting message archive from " + contactJid);
        EntityBareJid jid = null;

        MamManager mamManager = MamManager.getInstanceFor(connection);

        try {
            jid = JidCreate.entityBareFrom(contactJid);
        } catch (XmppStringprepException e) {
            e.printStackTrace();
        }

        //Nicht alle Server unterstützen es
        try {
            if(mamManager.isSupported()) {
                mamManager.enableMamForAllMessages();

                MamManager.MamQueryArgs mamQueryArgs = MamManager.MamQueryArgs.builder()
                        .limitResultsToJid(jid)
                        .setResultPageSizeTo(100)
                        .queryLastPage()
                        .build();
                MamManager.MamQuery mamQuery = mamManager.queryArchive(mamQueryArgs);
                List<Message> messages = mamQuery.getMessages();
                Log.d(TAG, "Got MAM " + messages.size());

                //TODO: bessere variante finden
                for(Message message : messages) {
                    Log.d(TAG, "message.getBody(): " + message.getBody());
                    Log.d(TAG, "message.getFrom(): " + message.getFrom());

                    //TODO: BUG: Beim holen der History ist die letzte Nachricht immer leer (+ hat davor nie existiert)
                    if(message.getBody() == null || message.getBody().isEmpty())
                        continue;

                    String from = message.getFrom().toString();
                    DelayInformation inf = message.getExtension(DelayInformation.ELEMENT,DelayInformation.NAMESPACE);

                    //Jetziges Datum
                    long time = (new Date()).getTime();
                    if(inf != null) {
                        time = inf.getStamp().getTime();
                    } else {
                        Log.d(TAG, "No DelayInfo");
                    }

                    String tempjid = "";
                    if(from.contains("/")) {
                        tempjid = from.split("/")[0];
                        Log.d(TAG, "The real jid is : " + tempjid);
                        Log.d(TAG, "The message is from: " + from);
                    }else {
                        tempjid = from;
                    }

                    //Bundle up intent and send the broadcast
                    Intent intent = new Intent(XMPPConnectionService.NEW_MESSAGE);
                    intent.setPackage(applicationContext.getPackageName());
                    intent.putExtra(XMPPConnectionService.BUNDLE_FROM_JID, tempjid);
                    intent.putExtra(XMPPConnectionService.BUNDLE_MESSAGE_BODY, message.getBody());
                    intent.putExtra(XMPPConnectionService.BUNDLE_TIME, time);

                    applicationContext.sendBroadcast(intent);
                }
            } else {
                Log.d(TAG, "MAM is not supported.");
            }
        } catch (SmackException.NoResponseException e) {
            e.printStackTrace();
        } catch (XMPPException.XMPPErrorException e) {
            e.printStackTrace();
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (SmackException.NotLoggedInException e) {
            e.printStackTrace();
        }
    }

    /**
     * Send every contact in the roster as a broadcast.
     * TODO: Maybe make something like a local buffer for the contacts
     */
    private void receiveContacts() {
        Log.d(TAG, "receiveContacts()");
        if(entries == null) {
            entries = roster.getEntries();
        }
        Log.d(TAG, "Roster has " + entries.size() + " entries.");
        for (RosterEntry entry : entries) {
            BareJid jid = entry.getJid();
            //Bundle up intent and send the broadcast
            Intent intent = new Intent(XMPPConnectionService.NEW_CONTACT);
            intent.setPackage(applicationContext.getPackageName());
            intent.putExtra(XMPPConnectionService.CONTACT_JID, jid.toString());
            if (entry.getName() != null)
                intent.putExtra(XMPPConnectionService.CONTACT_NICK, entry.getName());
            else
                intent.putExtra(XMPPConnectionService.CONTACT_NICK, jid.toString());
            try {
                VCard card = vcards.loadVCard(jid.asEntityBareJidOrThrow());
                if(card != null) {

                    byte[] avatar = card.getAvatar();
                    intent.putExtra(XMPPConnectionService.CONTACT_AVATAR, avatar);

                }else {
                    Log.d(TAG, "VCards not supported by " + jid);
                }
            } catch (SmackException.NoResponseException e) {
                e.printStackTrace();
            } catch (XMPPException.XMPPErrorException e) {
                e.printStackTrace();
            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "Sending " + jid.toString());
            applicationContext.sendBroadcast(intent);
        }
    }

    /**
     * Send a message to the specified target.
     * @param body
     * @param toJid
     */
    private void sendMessage(String body, String toJid) {
        Log.d(TAG, "Sending message to " + toJid);
        EntityBareJid jid = null;

        ChatManager chatManager = ChatManager.getInstanceFor(connection);

        try {
            jid = JidCreate.entityBareFrom(toJid);
        } catch(XmppStringprepException e) {
            e.printStackTrace();
        }

        Chat chat = chatManager.chatWith(jid);
        try {
            Message message = new Message(jid, Message.Type.chat);
            message.setBody(body);
            chat.send(message);
        } catch(SmackException.NotConnectedException e) {
            e.printStackTrace();
        } catch(InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void connected(org.jivesoftware.smack.XMPPConnection connection) {
        XMPPConnectionService.sConnectionState = ConnectionState.CONNECTED;
        Log.d(TAG, "Connected Successfully");
    }

    @Override
    public void authenticated(org.jivesoftware.smack.XMPPConnection connection, boolean resumed) {
        XMPPConnectionService.sConnectionState=ConnectionState.CONNECTED;
        Log.d(TAG,"Authenticated Successfully");
        showContactListActivityWhenAuthenticated();
    }

    @Override
    public void connectionClosed() {
        XMPPConnectionService.sConnectionState=ConnectionState.DISCONNECTED;
        Log.d(TAG,"Connectionclosed()");

        //Kill status notification
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(applicationContext);
        notificationManager.cancel(0);
    }

    @Override
    public void connectionClosedOnError(Exception e) {
        XMPPConnectionService.sConnectionState=ConnectionState.DISCONNECTED;
        Log.d(TAG,"ConnectionClosedOnError, error "+ e.toString());

        //Kill status notification
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(applicationContext);
        notificationManager.cancel(0);
    }
}
