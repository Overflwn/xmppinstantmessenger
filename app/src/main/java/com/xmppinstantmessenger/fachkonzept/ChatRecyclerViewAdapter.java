package com.xmppinstantmessenger.fachkonzept;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xmppinstantmessenger.R;
import com.xmppinstantmessenger.datenhaltung.BaseMessage;

import java.util.List;

public class ChatRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;

    private static final String TAG="ChatAdapter";

    private Context context;
    private List<BaseMessage> messageList;


    public ChatRecyclerViewAdapter(Context context, List<BaseMessage> messages) {
        this.context = context;
        this.messageList = messages;
        Log.d(TAG, "Creating Adapter, size: " + messages.size());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view;
        Log.d(TAG, "Creating ViewHolder");

        switch(viewType) {
            case VIEW_TYPE_MESSAGE_SENT:
                view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.chat_item_sent, viewGroup, false);
                return new SentMessageHolder(view);
            case VIEW_TYPE_MESSAGE_RECEIVED:
                view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.chat_item_received, viewGroup, false);
                return new ReceivedMessageHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        BaseMessage message = messageList.get(position);
        Log.d(TAG, "onBindViewHolder");
        switch(viewHolder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SENT:
                ((SentMessageHolder) viewHolder).bind(message);
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((ReceivedMessageHolder) viewHolder).bind(message);
        }
    }

    // Determines the appropriate ViewType according to the sender of the message.
    @Override
    public int getItemViewType(int position) {
        BaseMessage message = messageList.get(position);
        Log.d(TAG, "Getting viewType");
        //TODO: Check which user
        if (message.sender.nickname.equals("me")) {
            // If the current user is the sender of the message
            return VIEW_TYPE_MESSAGE_SENT;
        } else {
            // If some other user sent the message
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }
    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "Item count: " + messageList.size());
        return messageList.size();
    }

    public class ReceivedMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText, nameText;
        ImageView profileImage;

        ReceivedMessageHolder(View itemView) {
            super(itemView);
            Log.d(TAG, "Creating receivedMessageHolder");
            messageText = (TextView) itemView.findViewById(R.id.text_message_body);
            timeText = (TextView) itemView.findViewById(R.id.text_message_time);
            nameText = (TextView) itemView.findViewById(R.id.text_message_name);
            profileImage = (ImageView) itemView.findViewById(R.id.image_message_profile);
        }

        void bind(BaseMessage message) {
            messageText.setText(message.message);

            // Format the stored timestamp into a readable String using method.
            timeText.setText(DateUtils.formatDateTime(context, message.createdAt, DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME));
            nameText.setText(message.sender.nickname);

            // Insert the profile image from the URL into the ImageView.
            // Strings von der images Liste als Direktlinks zu Bilddateien interpretieren und runterladen
            /*Glide.with(context)
                    .asBitmap()
                    .load(message.sender.profileUrl)
                    .into(profileImage);*/
            if(message.sender.avatar != null) {
                profileImage.setImageBitmap(BitmapFactory.decodeByteArray(message.sender.avatar, 0, message.sender.avatar.length));
            }else {
                Glide.with(context)
                        .asBitmap()
                        .load(message.sender.profileUrl)
                        .into(profileImage);
            }


        }
    }

    public class SentMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText;

        SentMessageHolder(View itemView) {
            super(itemView);
            Log.d(TAG, "Creating sentMessageHolder");
            messageText = (TextView) itemView.findViewById(R.id.text_message_body);
            timeText = (TextView) itemView.findViewById(R.id.text_message_time);
        }

        void bind(BaseMessage message) {
            messageText.setText(message.message);

            // Format the stored timestamp into a readable String using method.
            timeText.setText(DateUtils.formatDateTime(context, message.createdAt, DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME));

        }
    }
}
