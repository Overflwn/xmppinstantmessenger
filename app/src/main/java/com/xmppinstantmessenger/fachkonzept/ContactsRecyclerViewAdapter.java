package com.xmppinstantmessenger.fachkonzept;

/**
* Bindet die Kontakte an die contact_item View Objekte.
*
* Author: Patrick Swierzy
* Zuletzt bearbeitet: 05.05.2019
*
* Notizen:
* - https://www.youtube.com/watch?v=Vyqz_-sJGFk
 */

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.xmppinstantmessenger.R;
import com.xmppinstantmessenger.XMPPConnectionService;
import com.xmppinstantmessenger.datenhaltung.BaseUser;
import com.xmppinstantmessenger.oberflaeche.ChatActivity;
import com.xmppinstantmessenger.oberflaeche.ContactListActivity;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ContactsRecyclerViewAdapter extends RecyclerView.Adapter<ContactsRecyclerViewAdapter.ViewHolder> {
    // Debugging
    private static final String TAG = "ContactsAdapter";

    // Der App-"Context" (-> keine Ahnung was das genau sein soll)
    private Context context;

    // Standard Konstruktor, wir benötigen den Context der App, welches diese Klasse (-> keine Activity)
    // nicht hat.
    public ContactsRecyclerViewAdapter( Context context) {
        this.context = context;
    }

    // "Fülle" die RecyclerView mit dem Inhalt auf
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    // View (contact_item) mit den entsprechenden Daten verbinden
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        Log.d(TAG, "onBindViewHolder: called.");

        // Strings von der images Liste als Direktlinks zu Bilddateien interpretieren und runterladen
        final BaseUser user = ContactListActivity.users.get(i);
        if(user.avatar != null) {
            Bitmap bm = BitmapFactory.decodeByteArray(user.avatar, 0, user.avatar.length);
            viewHolder.image.setImageBitmap(bm);
        } else {
            Glide.with(context)
                    .asBitmap()
                    .load(user.profileUrl)
                    .into(viewHolder.image);
        }

        // usernameView Text auf den Username einstellen
        viewHolder.username.setText(user.nickname);

        // Klickevent für die komplette View (contact_item)
        viewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked on: " + ContactListActivity.users.get(i).nickname);
                Toast.makeText(context, user.nickname, Toast.LENGTH_SHORT).show();
                // Starte "Chat"-Activity
                Intent intent = new Intent(context, ChatActivity.class);

                intent.putExtra("EXTRA_CONTACT_JID", user.jid);
                intent.putExtra("EXTRA_CONTACT_NICK", user.nickname);
                context.startActivity(intent);
                
            }
        });
    }

    public void deleteItem(int position) {
        Intent intent = new Intent(XMPPConnectionService.DELETE_CONTACT);
        intent.putExtra(XMPPConnectionService.CONTACT_JID, ContactListActivity.users.get(position).jid);
        context.sendBroadcast(intent);
        ContactListActivity.users.remove(position);
        notifyItemRemoved(position);
    }

    // Benötigt die RecyclerView zum funktionieren
    @Override
    public int getItemCount() {

        //return usernames.size();
        return ContactListActivity.users.size();
    }

    // Simpler ViewHolder, verbindet eine View (contact_item) mit den tatsächlichen Kontaktdaten
    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView image;
        TextView username;
        RelativeLayout parentLayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.imageView);
            username = itemView.findViewById(R.id.usernameView);
            parentLayout = itemView.findViewById(R.id.parent_layout);
        }
    }
}
