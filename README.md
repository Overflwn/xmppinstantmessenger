# XMPPInstantMessenger

Simple Instant Messenger using XMPP as backend (so it's basically a XMPP client) which uses SMACK as the XMPP library.

This app was made for a school project (~ 1/2 year time), so don't expect it to work flawlessly or even at all)

## Some known issues
- Sometimes the app crashes because vcards.isSupported throws an error telling me that the XMPPConnection is null, even after it successfully authorized (???)
- Most of the times smack thinks that VCards aren't supported even though in fact they are
	- If I comment out the vcards.isSupported expression it somehow (with a small chance) manages to load the vcards (???)
- It takes a long-ass time to authorize with the XMPP server (tested on jabber.de)
- I haven't implemented an error message when logging in with wrong credentials, hence the app gets stuck in the loading screen
- Sometimes the app crashes when adding a new contact but it actually adds it to the roster, which is why everything works when you restart the app
- I haven't implemented registering
- I haven't implemented editing your avatar, but it wouldn't work anyway as smack doesn't recognize VCard support properly
- Timestamps just don't work even though the server (jabber.de) supports them